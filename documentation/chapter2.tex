\section{Reakčně-difuzní rovnice}

Uvažujme úlohu
$$
    u_t = \gamma u_{xx} + \psi(u),
$$
kde $u \in \mathbb{R}$, $x \in \left[a, b\right]$, $t > 0$, s Dirichletovou okrajovou podmínkou $u(a,t) = \alpha$, Neumannovou okrajovou podmínkou $u_x(b, t) = \beta$ a počáteční podmínkou $u(x,0) = \phi(x)$. Úlohu transformujeme do tvaru, který je vhodný pro \uv{vložení} do numerického řešiče. Na prostorovou proměnnou využijeme metodu konečných diferencí, což nám úlohu transformuje na soustavu obyčejných diferenciálních rovnic, které budeme schopni vyřešit pomocí \textit{Octave~4.5.1}.

Např. v \cite{LeVeque} můžeme najít vztahy pro aproximace derivací poměrnými diferencemi. Pro aproximace prvních derivací použijeme centrální diferenci (\ref{eq:central_dif}) a pro aproximaci druhé derivace vztah (\ref{eq:second_dif}).

\begin{equation}
    D_Cu(\bar{x}) = \frac{u(\bar{x} - h) - u(\bar{x} + h)}{2h}
    \label{eq:central_dif}
\end{equation}

\begin{equation}
    D^2u(\bar{x}) = \frac{u(\bar{x} - h) - 2 u(\bar{x}) + u(\bar{x} + h)}{h^2}
    \label{eq:second_dif}
\end{equation}

Dále zvolíme ekvidistantní dělení intervalu $\left[a, b\right]$ s krokem $h_x$. Potom $m = \frac{b-a}{h_x} - 1$ označíme počet vnitřních uzlů. Interval $\left[a, b\right]$ tedy můžeme zapsat jako $\left[a, b\right] = \left[a, a + h_x\right] \cup \left[a + h_x, a + 2h_x\right] \cup \dots \cup \left[a + m h_x, b\right]$. Pro snadnější zápis označíme jednotlivé uzly intervalu $\left[a, b\right]$ jako $U_0, U_1, \dots, U_{m+1}$, kde $U_0 = a$ a $U_{m+1} = b$. Můžeme tedy psát $\left[a, b\right] = \bigcup_{i=0}^{m} \left[U_i, U_{i+1}\right]$.

Dostáváme
\begin{equation}
    (U_i)_t = \gamma \frac{U_{i-1} - 2U_i + U_{i+1}}{h_x^2} + \psi(U_i).
    \label{eq:disk_problem_2}
\end{equation}

Nyní se zaměříme na okrajové podmínky. Dirichletova okrajová podmínka nám zadává hodnotu přímo v bodě $a$ a promítne se do uzlu $U_1$. Samotnou rovnici $U_0 = \alpha$ využijeme až ve finálním vykreslení. Neumannovu okrajovou podmínku aproximujeme centrální diferencí, tj. použijeme (\ref{eq:central_dif}). Nemůžeme ji použít přímo, protože nemáme uzel $U_{m+2}$. V $U_{m+1}$ máme tyto rovnice
\begin{equation*}
    \begin{split}
        \frac{U_m - U_{m+2}}{2h} = \beta, \\
        \left(U_{m+1}\right)_t = \gamma \frac{U_m - 2U_{m+1} + U_{m+2}}{h_x^2} + \psi(U_{m+1})
    \end{split}
\end{equation*}
Z první rovnice vyjádříme $U_{m+2}$

\begin{equation*}
    U_{m+2} = U_m - 2h_x \beta
\end{equation*}
a dosadíme do druhé rovnice

\begin{equation}
    \left(U_{m+1}\right)_t = \gamma \frac{2U_m - 2U_{m+1}}{h_x^2} + \psi(U_{m+1}) - \frac{2h\beta\gamma}{h_x^2}.
    \label{eq:disk_neumann_boundary}
\end{equation}
Fiktivním bodům se říká \uv{ghost points}. Nyní máme vše potřebné pro přepsání úlohy do tvaru, který použijeme pro numerické řešení.

\subsubsection*{Soustava obyčejných diferenciálních rovnic}

Přístup, který jsme zvolili je ve smyslu metody přímek, kde prostorovou proměnnou aproximujeme konečnými diferencemi a čas necháme spojitý. Dostáváme tedy soustavu obyčejných diferenciálních rovnic.

Pro transformaci úlohy použijeme vztah (\ref{eq:disk_problem_2}). $U_0$ vynecháme, protože v něm je zadána Dirichletova okrajová podmínka. Upravíme tedy rovnici pro první uzel a přidáme rovnici pro poslední uzel. V $U_{m+1}$ musí být splněna Neumannova okrajová podmínka, tj. platí vztah (\ref{eq:disk_neumann_boundary}). Dostáváme

\begin{equation}
    \begin{bmatrix}
        U_1     \\
        U_2     \\
        \vdots  \\
        U_m     \\
        U_{m+1}
    \end{bmatrix}_t
    = \frac{\gamma}{h_x^2}
    \begin{bmatrix}
        -2 &      1 &    \  &     \  & \  \\
         1 &     -2 &     1 &     \  & \  \\
         \ & \ddots &\ddots & \ddots & \  \\
         \ &     \  &     1 &     -2 &  1 \\
         \ &     \  &    \  &      2 & -2
    \end{bmatrix}
    \begin{bmatrix}
        U_1     \\
        U_2     \\
        \vdots  \\
        U_m     \\
        U_{m+1}
    \end{bmatrix}
    +
    \begin{bmatrix}
        \psi(U_1) + \alpha / h_x^2    \\
        \psi(U_2)     \\
        \vdots        \\
        \psi(U_m)     \\
        \psi(U_{m+1}) - \frac{2h\beta\gamma}{h_x^2}
    \end{bmatrix}.
    \label{eq:final_disk}
\end{equation}

Po připojení počáteční podmínky, která je kompatibilní s okrajovými podmínkami tj. $\phi(U_0) = \alpha$ a $\phi'(U_{m+1}) = \beta$
\begin{equation}
    \begin{bmatrix}
        U_0(0) \\
        U_1(0) \\
        \vdots \\
        U_m(0) \\
        U_{m+1}(0)
    \end{bmatrix}
    =
    \begin{bmatrix}
        \phi(U_0) \\
        \phi(U_1) \\
        \vdots    \\
        \phi(U_m) \\
        \phi(U_{m+1})
    \end{bmatrix}
    \label{eq:final_disk_init}
\end{equation}
můžeme využít vztahy (\ref{eq:final_disk}) a (\ref{eq:final_disk_init}) k numerickým experimentům.

\subsection*{Konkrétní modely}

\subsubsection*{Populační model -- rovnice v 1D}

Jednou rovnicí se nám nepovede modelovat reakci dvou látek, ale můžeme tak modelovat například růst nějaké populace v čase. Jako nelinearitu zvolíme logistický člen, tj. $\psi(u) = u(1-u)$.

Budeme řešit dvě úlohy, jednu s Dirichletovými a jednu s Neumannovými okrajovými podmínkami. Analogickým postupem, který je popsán výše, převedeme úlohu (\ref{eq:num1}) do tvaru (\ref{eq:num1_t}) s kompatibilní počáteční podmínkou (\ref{eq:num1_i}) a úlohu (\ref{eq:num2}) do tvaru (\ref{eq:num2_t}) s kompatibilní počáteční podmínkou (\ref{eq:num2_i}). Numerické řešení úloh (\ref{eq:num1}) a (\ref{eq:num2}) je vykresleno v grafech na Obrázku \ref{figure:hom_dir_eq} a Obrázku \ref{figure:hom_neu_eq}. Z grafů vidíme, že úloha s homogenními Dirichletovými podmínkami se ústálí v $0$ a úloha s homogenními Neumannovými podmínkami konverguje k $1$, což jsou vlastně dva klidové stavy rovnice bez difuze (viz (\ref{eq:odr_stab})).

\begin{equation}
    \begin{split}
        u_t = u_{xx} + u(1 - u),\ x \in \left[0, 1\right],\ t \geq 0 \\
        u(0, t) = u(1, t) = 0 \\
        u(x, 0) = x(1 - x)
    \end{split}
    \label{eq:num1}
\end{equation}

\begin{equation}
    \begin{bmatrix}
        U_1         \\
        U_2         \\
        \vdots      \\
        U_{m-1}     \\
        U_m
    \end{bmatrix}_t
    = \frac{1}{h_x^2}
    \begin{bmatrix}
        -2 &      1 &    \  &     \  & \  \\
         1 &     -2 &     1 &     \  & \  \\
         \ & \ddots &\ddots & \ddots & \  \\
         \ &     \  &     1 &     -2 &  1 \\
         \ &     \  &    \  &      1 & -2
    \end{bmatrix}
    \begin{bmatrix}
        U_1         \\
        U_2         \\
        \vdots      \\
        U_{m-1}     \\
        U_m
    \end{bmatrix}
    +
    \begin{bmatrix}
        U_1 (1 - U_1)           \\
        U_2 (1 - U_2)           \\
        \vdots                  \\
        U_{m-1} (1 - U_{m-1})   \\
        U_m (1 - U_m)
    \end{bmatrix}
    \label{eq:num1_t}
\end{equation}

\begin{equation}
    \begin{bmatrix}
        U_1(0)      \\
        U_2(0)      \\
        \vdots      \\
        U_{m-1}(0)  \\
        U_m(0)
    \end{bmatrix}
    =
    \begin{bmatrix}
        U_1 (1 - U_1)           \\
        U_2 (1 - U_2)           \\
        \vdots                  \\
        U_{m-1} (1 - U_{m-1})   \\
        U_m (1 - U_m)
    \end{bmatrix}
    \label{eq:num1_i}
\end{equation}

\begin{equation}
    \begin{split}
        u_t = u_{xx} + u(1 - u),\ x \in \left[0, 1\right],\ t \geq 0 \\
        u_x(0, t) = u_x(1, t) = 0 \\
        u(x, 0) = \cos(\pi x) + 1
    \end{split}
    \label{eq:num2}
\end{equation}

\begin{equation}
    \begin{bmatrix}
        U_0     \\
        U_1     \\
        \vdots  \\
        U_m     \\
        U_{m+1}
    \end{bmatrix}_t
    = \frac{1}{h_x^2}
    \begin{bmatrix}
        -2 &      2 &    \  &     \  & \  \\
         1 &     -2 &     1 &     \  & \  \\
         \ & \ddots &\ddots & \ddots & \  \\
         \ &     \  &     1 &     -2 &  1 \\
         \ &     \  &    \  &      2 & -2
    \end{bmatrix}
    \begin{bmatrix}
        U_0     \\
        U_1     \\
        \vdots  \\
        U_m     \\
        U_{m+1}
    \end{bmatrix}
    +
    \begin{bmatrix}
        U_0 (1 - U_0)   \\
        U_1 (1 - U_1)   \\
        \vdots          \\
        U_m (1 - U_m)   \\
        U_{m+1} (1 - U_{m+1})
    \end{bmatrix}
    \label{eq:num2_t}
\end{equation}

\begin{equation}
    \begin{bmatrix}
        U_0(0)      \\
        U_1(0)      \\
        \vdots      \\
        U_m(0)      \\
        U_{m+1}(0)
    \end{bmatrix}
    =
    \begin{bmatrix}
        \cos(\pi U_0(0)) + 1    \\
        \cos(\pi U_1(0)) + 1    \\
        \vdots                  \\
        \cos(\pi U_m(0)) + 1    \\
        \cos(\pi U_{m+1}(0)) + 1
    \end{bmatrix}
    \label{eq:num2_i}
\end{equation}

\begin{figure}
    \centering
    \includegraphics[width=10cm]{picx/hom_bound_dirich_1d_eq}
    \caption{Populační rovnice s difuzí}
    \label{figure:hom_dir_eq}
\end{figure}

\begin{figure}
    \centering
    \includegraphics[width=10cm]{picx/hom_bound_neumann_1d_eq}
    \caption{Populační rovnice s difuzí}
    \label{figure:hom_neu_eq}
\end{figure}

\subsubsection*{Interakce chemických látek -- soustava dvou rovnic v 1D}

Pomocí dvou rovnic můžeme už modelovat vzájemnou interakci dvou chemických látek. Blíže se podíváme na tzv. Schnakenbergovu reakční kinetiku. Mějme chemickou reakci
\begin{equation*}
    \begin{split}
        X \xrightleftharpoons[\text{$K_{-1}$}]{\text{$K_1$}} A, \\
        B \xrightarrow{\text{$K_2$}} Y, \\
        2X + Y \xrightarrow{\text{$K_3$}} 3X,
    \end{split}
\end{equation*}
kde $X,Y,A,B$ jsou kladné koncentrace chemických látek a $k_{-1}, k_1, k_2, k_3$ jsou kladné rychlosti reakce. Překpokládáme-li, že koncentrace $X,Y$ se vyvíjí v čase a koncentrace $A,B$ jsou udržovány konstantní, můžeme pomocí zákona aktivních hmot odvodit systém obyčejných diferenciálních rovnic
\begin{equation*}
    \begin{split}
    X_t = k_{-1}A - k_1X + k_3 X^2 Y, \\
    Y_t = k_2 B - K_3 X^2Y,
    \end{split}   
\end{equation*}
který lze transformovat do bezrozměrného tvaru
\begin{equation*}
    \begin{split}
        u_t = a - u + u^2v, \\
        v_t = b - u^2v.
    \end{split}
\end{equation*}
Přidáme difuzi a dostáváme soustavu dvou reakčně-difuzních rovnic
\begin{equation}
    \begin{split}
        u_t = d_1 u_{xx} + a - u + u^2v, \\
        v_t = d_2 v_{xx} + b - u^2v,
    \end{split}
\end{equation}
kde $a,\ b > 0$ jsou parametry reakce a $d_1,\ d_2 > 0$ jsou difuzní koeficienty. Zvolíme $a = 0.2$, $b = 2$, $d_1 = 10^{-4}$ a $d_2 = 0.1$. Jako počáteční podmínku vygenerujeme \uv{náhodný} šum okolo konstanty $\left[\bar{u}, \bar{v}\right] = \left[ a + b, \frac{b}{(a + b^2)} \right]$ a opět prozkoumáme dva případy okrajových podmínek. Nehomogenní Dirichletovy viz Obrázek \ref{figure:dir_sys_1d}
\begin{equation*}
    \begin{split}
        u(0) = u(1) = \bar{u}, \\
        v(0) = v(1) = \bar{v}
    \end{split}
\end{equation*}
a homogenní Neumannovy, viz Obrázek \ref{figure:hom_neu_sys_1d}. V obou případech si můžeme všimnout, že pro $u$ i $v$ dostáváme periodická řešení. Dále prozkoumáme, jestli toto chování dostaneme i ve 2D.

\begin{figure}
    \centering
    \includegraphics[width=12cm]{picx/bound_dirichlet_1d_sys_u}
    \includegraphics[width=12cm]{picx/bound_dirichlet_1d_sys_v}
    \caption{Soustava dvou reakčně-difuzních rovnic v 1D}
    \label{figure:dir_sys_1d}
\end{figure}

\begin{figure}
    \centering
    \includegraphics[width=12cm]{picx/hom_bound_neumann_1d_sys_u}
    \includegraphics[width=12cm]{picx/hom_bound_neumann_1d_sys_v}
    \caption{Soustava dvou reakčně-difuzních rovnic v 1D}
    \label{figure:hom_neu_sys_1d}
\end{figure}


\subsubsection*{Interakce chemických látek -- soustava dvou rovnic ve 2D}

Mějme následující soustavu

\begin{equation}
    \begin{split}
        u_t = d_1 \Delta u + a - u + u^2v, \\
        v_t = d_2 \Delta v + b - u^2v.
    \end{split}
\end{equation}
Význam rovnic je stejný jako v jedné dimenzi. Jediný rozdíl je, že máme o jednu prostorovou proměnnou více. Úlohu řešíme na oblasti $\Omega = \left[ 0,1 \right] \times \left[ 0,1 \right]$. $\Omega$ diskretizujeme s krokem $h$, opět máme v jednom řádku $m + 2$ uzlů, tj. celá oblast má $(m+2)^2$ uzlů. Pro popis uzlů využijeme indexy $i, j = 0, \dots, m+1$, kde $i$ je řádkový index a $j$ je sloupcový index, tj. $U_{i,j}$ je uzel na $i$-tém řádku a $j$-tém sloupci. Uzly číslujeme postupně po řádcích zleva doprava, kde první řádek je na ose $x$ a poslední je v $y = 1$. Laplaceův operátor aproximujeme $5$ bodovým schématem.

\begin{equation*}
    \begin{split}
        \Delta u \approx \frac{U_{i,j-1} + U_{i-1,j} - 4U_{i,j} + U_{i+1,j} + U_{i,j+1}}{h^2} \\
        \Delta v \approx \frac{V_{i,j-1} + V_{i-1,j} - 4V_{i,j} + V_{i+1,j} + V_{i,j+1}}{h^2}
    \end{split}
\end{equation*}
Dostáváme soustavu ve tvaru

\begin{equation}
    \begin{split}
        (U_{i,j})_t = d_1 \frac{U_{i,j-1} + U_{i-1,j} - 4U_{i,j} + U_{i+1,j} + U_{i,j+1}}{h^2} + a - U_{i,j} + U_{i,j}^2V_{i,j}, \\
        (V_{i,j})_t = d_2 \frac{V_{i,j-1} + V_{i-1,j} - 4V_{i,j} + V_{i+1,j} + V_{i,j+1}}{h^2} + b - U_{i,j}^2V_{i,j}.
    \end{split}
    \label{eq:2d_neumann}
\end{equation}
Nyní se zaměříme jen na homogenní Neumannovy okrajové podmínky.

\begin{equation*}
    \begin{split}
        u(0, y, t)_{-x} = u(1, y, t)_x = u(x, 0, t)_{-y} = u(x, 1, t)_y = 0, \\
        v(0, y, t)_{-x} = v(1, y, t)_x = v(x, 0, t)_{-y} = v(x, 1, t)_y = 0.
    \end{split}
\end{equation*}
Opět použijeme myšlenku tzv. \uv{ghost points}. Do problémů se dostaneme v rozích, tj. v $U_{0, 0}, U_{m+1, 0}, U_{0, m+1}$ a $U_{m+1, m+1}$, protože v nich neexistuje derivace podle vnější normály. Využijeme tedy derivace ve směrech $x$ a $y$. Neumannovy okrajové podmínky v těchto bodech aproximujeme následovně. Pro $U_{0,0}$ a $V_{0,0}$ dostáváme

\begin{equation*}
    \begin{split}
        \frac{U_{1,0} - U_{-1,0}}{2h} = 0 \Rightarrow U_{1,0} = U_{-1,0}, \\
        \frac{U_{0,1} - U_{0,-1}}{2h} = 0 \Rightarrow U_{0,1} = U_{0,-1}, \\
        \frac{V_{1,0} - V_{-1,0}}{2h} = 0 \Rightarrow V_{1,0} = V_{-1,0}, \\
        \frac{V_{0,1} - V_{0,-1}}{2h} = 0 \Rightarrow V_{0,1} = V_{0,-1}.
    \end{split}
\end{equation*}
Dosadíme do (\ref{eq:2d_neumann}) a získáme rovnice pro první uzel

\begin{equation}
    \begin{split}
        (U_{0,0})_t = d_1 \frac{-4U_{0,0} + 2U_{0,1} + 2U_{1,0}}{h^2} + a + U_{0,0} + U_{0,0}^2V_{0,0}, \\
        (V_{0,0})_t = d_1 \frac{-4V_{0,0} + 2V_{0,1} + 2V_{1,0}}{h^2} + b - U_{0,0}^2V_{0,0}.
    \end{split}
\end{equation}
Obdobně se udělají zbylé tři rohy.

Posledním krokem je poskládat jednotlivé části nějakým rozumným způsobem do matice, abychom ji mohli opět vložit do numerického řešiče. Jeden ze způsobů je, že si napíšeme lokální matici pro řádky. Lokální matici potom poskládáme blokově na diagonálu globální matice, ve které bude navíc nad a pod identická matice, kde v první a poslední blok identické matice bude přenásoben $2$. Globální matice je tedy blokově $3$ diagonální. Konkrétně tedy lokální matice je ve tvaru

\begin{equation}
    \textbf{L} =
    \begin{bmatrix}
        -4 &  2     & \      & \      & \  \\
         1 & -4     & 1      & \      & \  \\
         \ & \ddots & \ddots & \ddots & \  \\
         \ & \      & 1      & -4     &  1 \\
         \ & \      & \      &  2     & -4
    \end{bmatrix},
\end{equation}
a globální matice je ve tvaru

\begin{equation}
    \textbf{G} =
    \frac{1}{h^2}
    \begin{bmatrix}
        \textbf{P}  & 2 \textbf{I} & \          & \            & \          \\
         \textbf{I} & \textbf{P}   & \textbf{I} & \            & \          \\
         \          & \ddots       & \ddots     & \ddots       & \          \\
         \          & \            & \textbf{I} & \textbf{P}   & \textbf{I} \\
         \          & \            & \          & 2 \textbf{I} & \textbf{P}
    \end{bmatrix}.
\end{equation}

Přenásobení identické matice v prvním a posledním řádku matice $\textbf{G}$ nám řeší okrajové podmínky pro body $U_{1,0}, \dots, U_{m,0}$ a $U_{m+1,1}, \dots, U_{m+1,m}$. Dvojky v lokální matici jsou analogické problému v 1D tedy řeší okrajové podmínky pro body $U_{0,1}, \dots, U_{0, m}$ a $U_{m+1, 1}, \dots, U_{m+1,m}$. \uv{Kombinace} $\textbf{G}$ a $\textbf{L}$ řeší i okrajové podmínky v rozích.

Diskretizovaná úloha je tedy
\begin{equation}
    \begin{split}
        U_t = d_1 \textbf{G}U + \psi_u(U,V), \\
        V_t = d_2 \textbf{G}U + \psi_v(U,V),
    \end{split}
\end{equation}
kde $U, V \in \mathbb{R}^{(m+2)^2}$, $\textbf{G} \in \mathbb{R}^{(m+2)^2 \times (m+2)^2}$ a $\psi_u, \psi_v \in \mathbb{R}^{(m+2)^2}$ jsou příslušné nelinearity. Počáteční podmínku volíme stejně jako v 1D.

Výsledky výpočtů jsou zobrazeny v 3D grafech, na které se koukáme shora, viz Obrázek \ref{figure:2d_system}. Podobně jako v 1D případě dostáváme periodická řešení.
\begin{figure}
    \centering
    \includegraphics[height=8cm]{picx/hom_neumann_2d_sol_u}
    \includegraphics[height=8cm]{picx/hom_neumann_2d_sol_v}
    \caption{Soustava dvou reakčně-difuzních rovnic ve 2D}
    \label{figure:2d_system}
\end{figure}