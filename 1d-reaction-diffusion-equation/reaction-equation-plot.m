clc
clear
close all

f = inline('u*(1-u)','t','u');
#odeopt = odeset ('RelTol', 0.00001, 'AbsTol', 0.00001,'InitialStep',0.5,'MaxStep',0.5);

[t1,u1] = ode45(f, [0,5], 1/10);
[t2,u2] = ode45(f, [0,5], 1/2);
[t3,u3] = ode45(f, [0,5], 3/2);

figure(1)
plot(t1, u1, "linewidth", 1, "color", "k")
hold on
plot(t2, u2, "linewidth", 1, "color", "k")
hold on
plot(t3, u3, "linewidth", 1, "color", "k")
hold on
line ([0 5], [1 1], "linewidth", 1.5, "linestyle", "-", "color", "r");
line ([0 5], [0 0], "linewidth", 1.5, "linestyle", "--", "color", "r");

title("Solution for different initial conditions, k = 1, q = 1")
xlabel("t")
ylabel("u")
axis([0, 5, 0, 3/2])