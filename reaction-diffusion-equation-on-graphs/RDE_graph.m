

clc
clear
close all

a = 0.5;% saturation param.
L = 3.6;% reaction param.
n = 1000;% number of nodes
A = sparse(n, n);

# matrix assemble
vectorized = zeros(n,3);
vectorized(1:n,1) = -2;
vectorized(1:end-1,2) = 1;
vectorized(2:end,3) = 1;
diagVek = [0 -1 1];    
A = spdiags(vectorized,diagVek,n,n);
A(n,1)=1;
A(1,n)=1;

% initial cond.
xplot = 0:(1/(n-1)):1;
f = 10;
ic = (sin(2*pi*f*xplot)+1)/2;


function rhs = odefun(t, u, a, L, A)
  rhs = A*u + L*u.*(1-u).*(u-a);
endfunction

t_cmp = [0, 10^6]
[t,sol] = ode15s(@(t,u) odefun(t, u, a, L, A), t_cmp, ic);

xplot = 0:(1/(n-1)):1;
tplot = ceil(size(t)(1) / (t_cmp(2) - t_cmp(1)) * 0.1)
for it = 1:tplot:size(t)(1)
  printf("t = %f\n", t(it))
  scatter(xplot, sol(it, :), 'filled')
  axis ([0 1 0 1])
  str = sprintf("Solution in t = %f", t(it)); 
  title(str)
  xlabel("x")
  ylabel("u(x,t)")
  pause
endfor